#!/bin/bash
###### Variables #######
user="datalink"

##############################
########## Paso 1 ############
##############################

#---> /etc/apt/sources.list

sed -i 's/deb cdrom/#deb cdrom:/g' /etc/apt/sources.list

#--> sudo /etc/sudoers

sudo usermod -a -G sudo  $user
sed -i 's/%sudo	ALL=(ALL:ALL) ALL/sudo	ALL=(ALL:ALL) ALL/g' /etc/sudoers
echo $user "ALL=(ALL:ALL) ALL" >> /etc/sudoers

#---> paquetes adicionales
#su datalink &
sudo apt install vim wget tree expect git curl gpg -y
sudo apt update 
sudo apt upgrade -y 

##############################
########## Paso 2 ############
##############################

sudo apt install build-essential manpages-dev gcc gnupg2 -y
sudo gcc --version

##############################
########## Paso 3 ############
##############################

tar -vxf zeromq-3.2.1-rc2.tar.gz
sudo chown datalink:datalink -R zeromq-3.2.1
cd zeromq-3.2.1
./configure
make
sudo make install
sudo ldconfig
cd /home/$user
rm zeromq-3.2.1-rc2.tar.gz

##############################
########## Paso 4 ############
##############################

tar -vxf protobuf-all-3.19.4.tar
sudo chown datalink:datalink -R protobuf-all-3.19.4
cd protobuf-all-3.19.4.tar
./configure
make
sudo make install
sudo ldconfig
cd /home/$user
rm protobuf-all-3.19.4.tar

##############################
########## Paso 5 ############
##############################

sudo apt install redis -y

##############################
########## Paso 6 ############
##############################

cd /etc/apt/sources.list.d/
touch mongodb-org-5.0.list
echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" >> mongodb-org-5.0.list

curl -sSL https://www.mongodb.org/static/pgp/server-5.0.asc  -o mongoserver.asc

gpg --no-default-keyring --keyring ./mongo_key_temp.gpg --import ./mongoserver.asc
gpg --no-default-keyring --keyring ./mongo_key_temp.gpg --export > ./mongoserver_key.gpg

mv mongoserver_key.gpg /etc/apt/trusted.gpg.d/

apt update
apt install mongodb-org -y
systemctl enable --now mongod
mongod --version
cd /home/$user

##############################
########## Paso 7 ############
##############################
sudo apt install -y openjdk-11-jdk
java --version

##############################
########## Paso 9 ############
##############################
#cd /home/$user
#wget https://download.jetbrains.com/idea/ideaIC-2022.2.3.tar.gz
#tar -zxvf ideaIC-2022.2.3.tar.gz
#mkdir  /opt/idea
#chmod 777 -R /opt/idea
#mv idea-*/*  /opt/idea
#cd /opt/idea/bin/
#sh idea.sh
#cd /home/$user
#rm ideaIC-2022.2.3.tar.gz


##############################
########## Paso 10 ###########
##############################

curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
#node --version
#npm --version 
sudo npm install pm2 -g
#sudo reboot

##############################
########## Paso 8 ############
##############################

#sudo apt install libxcb-xinerama0 libglu1-mesa-dev freeglut3-dev libgl-dev -y
#sudo apt-get install -y libxcb-icccm4  libxcb-image0 libxcb-keysyms1-dev libxcb-render-util0 libxcb-xkb-dev -y 
#sudo apt install build-essential checkinstall zlib1g-dev libssl-dev -y
#wget https://github.com/Kitware/CMake/releases/download/v3.22.2/cmake-3.22.2.tar.gz
#tar -zxvf cmake-3.22.2.tar.gz
#cd cmake-3.22.2
#./bootstrap
#make
#make install
#cd /home/$user
#rm cmake-3.22.2.tar.gz

#sudo wget https://qt.mirror.constant.com/archive/qt/6.2/6.2.3/single/qt-everywhere-src-6.2.3.tar.xz 
#tar -Jxvf qt-everywhere-src-6.2.3.tar.xz
#cd qt-everywhere-src-6.2.3
#./configure -opensource -confirm-license
#gmake
#gmake install
#cd /home/datalink
#rm qt-everywhere-src-6.2.3.tar.xz
#sudo reboot